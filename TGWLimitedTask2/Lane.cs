﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TGWLimitedTask2
{
    public class Lane
    {
        public int LoadTotal { get; set; }
        public List<int> Loads { get; set; }

        public Lane()
        {
            this.LoadTotal = 0;
            this.Loads = new List<int>();
        }

        /// <summary>
        /// Returns percentage of loads that reached their destination
        /// </summary>
        /// <returns></returns>
        public double ReachedPercentage()
        {
            return Math.Round((double)Loads.Count / LoadTotal * 100, 3);
        }

        public string LoadsToString()
        {
            string result = "";
            foreach(int item in Loads)
            {
                result = result + " " + item;
            }

            return result;
        }
    }
}
