﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace TGWLimitedTask2
{
    internal class Program
    {
        static void Main(string[] args)
        {
            int availableDestinationCount = 0;
            int destinationStrategy = 0;
            int consecutiveLoads = 0;
            double failurePercentage = 0;
            int numberOfLoads = 0;

            // Get all variables from user input
            Dialog(out availableDestinationCount, out destinationStrategy, out consecutiveLoads, out failurePercentage, out numberOfLoads);

            // Start the calculation based on the selected strategy
            switch (destinationStrategy)
            {
                case 0:
                    RoundRobinStrategy(availableDestinationCount, consecutiveLoads, failurePercentage, numberOfLoads);
                    break;

                case 1:
                    RandomStrategy(availableDestinationCount, consecutiveLoads, failurePercentage, numberOfLoads);
                    break;

                default:
                    Console.WriteLine("Selected strategy is incorrect!");
                    break;
            }
        }

        private static void RandomStrategy(int availableDestinationCount, int consecutiveLoads, double failurePercentage, int numberOfLoads)
        {
            // List of lanes
            List<Lane> lanes = new List<Lane>();
            for (int i = 0; i <= availableDestinationCount; i++) lanes.Add(new Lane());

            // Load stack
            Stack<int> loads = new Stack<int>();
            for (int i = numberOfLoads; i >= 1; i--) loads.Push(i);

            // Route the load
            while (loads.Count > 0)
            {
                // Pop n amount of times (based on 'consecutiveLoads')
                for (int i = 0; i < consecutiveLoads; i++)
                {
                    // Break if no more load in stack
                    if (loads.Count == 0) break;

                    // Get random lane
                    int laneIndex = GetRandomLane(availableDestinationCount);

                    // Pop value from stack
                    int load = loads.Pop();
                    lanes[laneIndex].LoadTotal++;
                    lanes[0].LoadTotal++;

                    // Check if load fails to be diverted based on given failure chance
                    if (hasFailedToDivert(failurePercentage)) lanes[0].Loads.Add(load);
                    else lanes[laneIndex].Loads.Add(load);
                }
            }
            
            // Print result
            PrintLanes(lanes);
        }

        private static void RoundRobinStrategy(int availableDestinationCount, int consecutiveLoads, double failurePercentage, int numberOfLoads)
        {
            // List of lanes
            List<Lane> lanes = new List<Lane>();
            for (int i = 0; i <= availableDestinationCount; i++) lanes.Add(new Lane());

            // Load stack
            Stack<int> loads = new Stack<int>();
            for (int i = numberOfLoads; i >= 1; i--) loads.Push(i);

            // Route the load
            int laneIndex = 1;
            while (loads.Count > 0)
            {
                // Pop n amount of times (based on 'consecutiveLoads')
                for (int i = 0; i < consecutiveLoads; i++)
                {
                    // Break if no more load in stack
                    if (loads.Count == 0) break;

                    // Pop value from stack
                    int load = loads.Pop();
                    lanes[laneIndex].LoadTotal++;
                    lanes[0].LoadTotal++;

                    // Check if load fails to be diverted based on given failure chance
                    if (hasFailedToDivert(failurePercentage)) lanes[0].Loads.Add(load);
                    else lanes[laneIndex].Loads.Add(load);
                }

                if (laneIndex == availableDestinationCount) laneIndex = 1;
                else laneIndex++;
            }

            // Print result
            PrintLanes(lanes);
        }

        private static void PrintLanes(List<Lane> lanes)
        {
            // Print result
            for (int i = 0; i < lanes.Count; i++)
            {
                Console.WriteLine("[" + i + " destination] " +
                    lanes[i].Loads.Count + "/" + lanes[i].LoadTotal +
                    " " + lanes[i].ReachedPercentage() + "%");
            }
        }

        private static bool hasFailedToDivert(double failurePercentage)
        {
            Random random = new Random();
            double randomValue = random.NextDouble() * 100;

            if (randomValue > 0 && randomValue <= failurePercentage) return true;
            else return false;
        }

        private static int GetRandomLane(int availableDestinationCount)
        {
            Random random = new Random();

            return random.Next(1, availableDestinationCount + 1);
        }

        private static void Dialog(out int availableDestinationCount, out int destinationStrategy, out int consecutiveLoads, out double failurePercentage, out int numberOfLoads)
        {
            Console.Write("Number of destinations (0-n): ");
            availableDestinationCount = int.Parse(Console.ReadLine());

            Console.WriteLine("\n0 - Round robin \n1 - Random");
            Console.Write("Select strategy: ");
            destinationStrategy = int.Parse(Console.ReadLine());

            Console.Write("\nNumber of consecutive loads: ");
            consecutiveLoads = int.Parse(Console.ReadLine());

            Console.Write("\nPercentage of failure: ");
            failurePercentage = double.Parse(Console.ReadLine());

            Console.Write("\nNumber of loads: ");
            numberOfLoads = int.Parse(Console.ReadLine());

            Console.WriteLine();
        }
    }
}
